﻿using Microsoft.EntityFrameworkCore;

namespace OmegaBlare.Core
{
    public class Data : DbContext
    {
        public Data(DbContextOptions<Data> options) : base(options) { }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}