﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OmegaBlare.Core
{
        [Table("product")]
        public class Product
        {
            [Key, Required]
            public int id { get; set; }
            public string Model { get; set; } = string.Empty;
            public string Brand { get; set; } = string.Empty;
            public int Techspec { get; set; }
            public decimal Price { get; set; }
        }
}
