﻿using OmegaBlare.Core;

namespace OmegaBlare.Model
{
    public class DbAshi
    {
        private Data _context;
        public DbAshi(Data context)
        {
            _context = context;
        }
        /// <summary>
        /// GET
        /// </summary>
        /// <returns></returns>
        public List<ProductModel> GetProducts()
        {
            List<ProductModel> response = new List<ProductModel>();
            var dataList = _context.Products.ToList();
            dataList.ForEach(row => response.Add(new ProductModel()
            {
                Brand = row.Brand,
                id = row.id,
                Model = row.Model,
                Price = row.Price,
                Techspec = row.Techspec
            }));
            return response;
        }

        public ProductModel GetProductById(int id)
        {
            ProductModel response = new ProductModel();
            var row = _context.Products.Where(d => d.id.Equals(id)).FirstOrDefault();
            return new ProductModel()
            {
                Brand = row.Brand,
                id = row.id,
                Model = row.Model,
                Price = row.Price,
                Techspec = row.Techspec
            };
        }
        /// <summary>
        /// It serves the POST/PUT/PATCH
        /// </summary>
        public void SaveOrder(OrderModel orderModel)
        {
            Order dbTable = new Order();
            if (orderModel.id > 0)
            {
                //PUT
                dbTable = _context.Orders.Where(d => d.id.Equals(orderModel.id)).FirstOrDefault();
                if (dbTable != null)
                {
                    dbTable.Phone = orderModel.Phone;
                    dbTable.Address = orderModel.Address;
                }
            }
            else
            {
                //POST
                dbTable.Phone = orderModel.Phone;
                dbTable.Address = orderModel.Address;
                dbTable.Name = orderModel.Name;
                dbTable.Product = _context.Products.Where(f => f.id.Equals(orderModel.Product_id)).FirstOrDefault();
                _context.Orders.Add(dbTable);
            }
            _context.SaveChanges();
        }
        /// <summary>
        /// DELETE
        /// </summary>
        /// <param name="id"></param>
        public void DeleteOrder(int id)
        {
            var order = _context.Orders.Where(d => d.id.Equals(id)).FirstOrDefault();
            if (order != null)
            {
                _context.Orders.Remove(order);
                _context.SaveChanges();
            }
        }
    }
}
