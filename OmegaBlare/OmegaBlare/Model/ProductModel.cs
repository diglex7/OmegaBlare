﻿namespace OmegaBlare.Model
{
    public class ProductModel
    {
        public int id { get; set; }
        public string Model { get; set; } = string.Empty;
        public string Brand { get; set; } = string.Empty;
        public int Techspec { get; set; }
        public decimal Price { get; set; }
    }
}
