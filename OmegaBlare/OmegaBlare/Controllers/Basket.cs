﻿using Microsoft.AspNetCore.Mvc;
using OmegaBlare.Core;
using OmegaBlare.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OmegaBlare.Controllers
{

    [ApiController]
    public class BasketController : ControllerBase
    {
        private readonly DbAshi _db;
        public BasketController(Data Data)
        {
            _db = new DbAshi(Data);
        }
        // GET: api/<BasketController>
        [HttpGet]
        [Route("api/[controller]/GetProducts")]
        public IActionResult Get()
        {
            ResponseType type = ResponseType.Success;
            try
            {
                IEnumerable<ProductModel> data = _db.GetProducts();

                if (!data.Any())
                {
                    type = ResponseType.NotFound;
                }
                return Ok(BrakeResponse.GetAppResponse(type, data));
            }
            catch (Exception ex)
            {
                return BadRequest(BrakeResponse.GetExceptionResponse(ex));
            }
        }
        // GET api/<BasketController>/5
        [HttpGet]
        [Route("api/[controller]/GetProductById/{id}")]
        public IActionResult Get(int id)
        {
            ResponseType type = ResponseType.Success;
            try
            {
                ProductModel data = _db.GetProductById(id);

                if (data == null)
                {
                    type = ResponseType.NotFound;
                }
                return Ok(BrakeResponse.GetAppResponse(type, data));
            }
            catch (Exception ex)
            {
                return BadRequest(BrakeResponse.GetExceptionResponse(ex));
            }
        }

        // POST api/<BasketController>
        [HttpPost]
        [Route("api/[controller]/SaveOrder")]
        public IActionResult Post([FromBody] OrderModel model)
        {
            try
            {
                ResponseType type = ResponseType.Success;
                _db.SaveOrder(model);
                return Ok(BrakeResponse.GetAppResponse(type, model));
            }
            catch (Exception ex)
            {
                return BadRequest(BrakeResponse.GetExceptionResponse(ex));
            }
        }

        // PUT api/<BasketController>/5
        [HttpPut]
        [Route("api/[controller]/UpdateOrder")]
        public IActionResult Put([FromBody] OrderModel model)
        {

            try
            {
                ResponseType type = ResponseType.Success;
                _db.SaveOrder(model);
                return Ok(BrakeResponse.GetAppResponse(type, model));
            }
            catch (Exception ex)
            {
                return BadRequest(BrakeResponse.GetExceptionResponse(ex));
            }
        }

        // DELETE api/<BasketController>/5
        [HttpDelete]
        [Route("api/[controller]/DeleteOrder/{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                ResponseType type = ResponseType.Success;
                _db.DeleteOrder(id);
                return Ok(BrakeResponse.GetAppResponse(type, "Delete Successfully"));
            }
            catch (Exception ex)
            {
                return BadRequest(BrakeResponse.GetExceptionResponse(ex));
            }
        }
    }
}
