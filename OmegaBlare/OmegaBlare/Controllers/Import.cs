﻿using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OmegaBlare.Model;

namespace OmegaBlare.Controllers
{
    public class Import : Controller
    {
        [HttpGet]
        [Route("api/upload/[controller]/Import")]
        public ActionResult upload()
        {
            return View();
        }
        public async Task<List<ProductModel>> import(IFormFile file)
        {
            var list = new List<ProductModel>();
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowcount = worksheet.Dimension.Rows;
                    for (int row = 2; row <= rowcount; row++)
                    {
                        list.Add(new ProductModel
                        {
                             id = Convert.ToInt32(worksheet.Cells[row, 1].Value.ToString().Trim()),
                             Model = worksheet.Cells[row, 2].Value.ToString().Trim(),
                             Brand = worksheet.Cells[row, 3].Value.ToString().Trim(),
                             Techspec = Convert.ToInt32(worksheet.Cells[row, 4].Value.ToString().Trim()),
                             Price = Convert.ToInt32(worksheet.Cells[row, 5].Value.ToString().Trim()),
                        });
                    }
                }
            }
            return list;
        }
    }
}
